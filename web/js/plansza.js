let twig = document.getElementsByClassName('js-user-rating');
let haslo = twig[0].getAttribute('data-get-haslo');
haslo = haslo.toUpperCase();

let trafionyS = new Audio('../sound/yes.wav');
let pudloS = new Audio('../sound/no.wav');
let wygranaS = new Audio('../sound/tada.wav');
let przegranaS = new Audio('../sound/boo.wav');

let plansza_uchwyt = document.getElementById("plansza");
plansza_uchwyt.innerHTML = ukryj_haslo(haslo);

function ukryj_haslo(h){
	let wynik = "";
	for(let i=0; i<h.length; i++){
		let znak = h.charAt(i);
		if(znak == " "){
			wynik += " ";
		}else{
			wynik += "-";
		}
	}
	return wynik;
}


function sprawdz_znak(znak, id_przycisku){
	let plansza = plansza_uchwyt.innerHTML;
	let h = haslo;
	let nowe_haslo = "";
	let czy_trafiony = false;
	for(let i=0; i<h.length;i++){
		if(h.charAt(i) == " "){
			nowe_haslo +=" ";
		}else if((h.charAt(i) == znak)){
			nowe_haslo += znak;
			czy_trafiony = true;
		}else if(plansza.charAt(i)!="-"){
			nowe_haslo +=plansza.charAt(i);
	}else{
			nowe_haslo +="-";
		}
	}
		if(!czy_trafiony){
			console.log("pudło");
			pudloS.play();
			buduj_szubience();
		}else{
			console.log("trafiłeś");
			trafionyS.play();
		}
		deaktywuj_przycisk(id_przycisku, czy_trafiony);
		sprawdz_czy_odkryte(nowe_haslo);
		plansza_uchwyt.innerHTML = nowe_haslo;
}

function sprawdz_czy_odkryte(n_haslo){
	if(n_haslo.indexOf("-")==-1){
		koncz_gre(true);
	}
	return 0;
}

function buduj_szubience(){
	let img = document.getElementById("szubienica").firstChild.nextSibling;
	let src = img.getAttribute("src");
	let koniecpliku = src.match(/s([0-9])\.jpg/);
	let nr_foto = parseInt(koniecpliku[1])+1;
	let nr_foto_string = "s"+nr_foto.toString()+".jpg";
	let nowy_src = src.replace(koniecpliku[0], nr_foto_string);
	console.log(nowy_src);
	img.setAttribute('src', nowy_src);
	if(nr_foto==9){
		console.log("wieceij nie bedzie");
		koncz_gre(false);
	}
}

function koncz_gre(jaki_koniec){
	let alfabet = document.getElementById("alfabet");
	let text;
	if(!jaki_koniec){
	text = 'PRZEGRAŁEŚ!<br>CHCESZ SPRÓBOWAĆ JESZCZE RAZ?<br>';
	//text += '<span class="szpan" onclick="location.reload();">KLIKNIJ TUTAJ</span>';
	przegranaS.play();
	}else{
	text = 'WYGRAŁEŚ!<br>CHCESZ SPRÓBOWAĆ JESZCZE RAZ?<br>';
	wygranaS.play();
	}
	text += '<a class="szpan" href="http://localhost/symfony_nauka/wisielec_symfony/web/app_dev.php/">KLIKNIJ TUTAJ</a>';
	alfabet.innerHTML = text;
}