wyswietl_alfabet();

function wyswietl_alfabet(){
let alfabet = new Array("A","Ą", "B", "C", "Ć", "D", "E", "Ę", "F", "G", "H", "I", "J", "K", "L", "Ł",
		"M", "N", "Ń", "O", "Ó", "P", "Q", "R", "S", "Ś", "T", "U", "V", "W", "X", "Y", "Z", "Ź", "Ż");

let alfabet_uchwyt = document.getElementById("alfabet");
let ile_liter = alfabet.length;
for(let i=0; i<ile_liter; i++){
	let div = document.createElement('div');
	div.innerHTML = alfabet[i];
	div.setAttribute("id", "lit"+i);
	div.setAttribute("class", "litera");
	div.setAttribute("onclick", 'sprawdz_znak("'+alfabet[i]+'", '+i+')');
	alfabet_uchwyt.appendChild(div);
	if((i+1)%7==0){
		let div_clear = document.createElement('div');
		div_clear.setAttribute("style", "clear:both;");
		alfabet_uchwyt.appendChild(div_clear);
	}
}
}

function deaktywuj_przycisk(id_przycisku, czy_trafiony){
	let chwyt_litery = document.getElementById("lit"+id_przycisku.toString());
	if(czy_trafiony){
		chwyt_litery.setAttribute("style", "border: 3px solid lightgreen; color: lightgreen; background: rgb(44, 82, 56) none repeat scroll 0% 0%;");
	}else{
		chwyt_litery.setAttribute("style", "border: 3px solid pink; color: pink; background: rgb(51, 0, 0) none repeat scroll 0% 0%; cursor: default;");
		chwyt_litery.setAttribute("onclick", ";");
	}
}