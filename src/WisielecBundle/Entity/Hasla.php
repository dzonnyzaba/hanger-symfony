<?php

namespace WisielecBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hasla
 *
 * @ORM\Table(name="hasla")
 * @ORM\Entity(repositoryClass="WisielecBundle\Repository\HaslaRepository")
 */
class Hasla
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_kategorii", type="integer")
     */
    private $idKategorii;

    /**
     * @var string
     *
     * @ORM\Column(name="haslo", type="string", length=255)
     */
    private $haslo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idKategorii
     *
     * @param integer $idKategorii
     *
     * @return Hasla
     */
    public function setIdKategorii($idKategorii)
    {
        $this->idKategorii = $idKategorii;

        return $this;
    }

    /**
     * Get idKategorii
     *
     * @return int
     */
    public function getIdKategorii()
    {
        return $this->idKategorii;
    }

    /**
     * Set haslo
     *
     * @param string $haslo
     *
     * @return Hasla
     */
    public function setHaslo($haslo)
    {
        $this->haslo = $haslo;

        return $this;
    }

    /**
     * Get haslo
     *
     * @return string
     */
    public function getHaslo()
    {
        return $this->haslo;
    }
}

