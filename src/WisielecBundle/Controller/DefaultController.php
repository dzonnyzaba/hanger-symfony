<?php

namespace WisielecBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="welcome")
     */
    public function indexAction()
    {
        return $this->render('WisielecBundle:Default:index.html.twig');
    }
	
	/**
     * @Route("/przyslowia", name="przyslowia")
     */
    public function przyslowiaAction()
    {
		$repository = $this->getDoctrine()->getRepository('WisielecBundle:Hasla');
		$hasla = $repository->findBy(array('idKategorii'=>1));
		$wylosowane = $hasla[array_rand($hasla, 1)];
		
        return $this->render('WisielecBundle:Default:gra.html.twig', array('wylosowane'=>$wylosowane));
    }
	
	/**
     * @Route("/aktorzy", name="aktorzy")
     */
    public function aktorzyAction()
    {
		$repository = $this->getDoctrine()->getRepository('WisielecBundle:Hasla');
		$hasla = $repository->findBy(array('idKategorii'=>2));
		$wylosowane = $hasla[array_rand($hasla, 1)];
        return $this->render('WisielecBundle:Default:gra.html.twig', array('wylosowane'=>$wylosowane));
    }
	
	/**
     * @Route("/pisarze", name="pisarze")
     */
    public function pisarzeAction()
    {
		$repository = $this->getDoctrine()->getRepository('WisielecBundle:Hasla');
		$hasla = $repository->findBy(array('idKategorii'=>3));
		$wylosowane = $hasla[array_rand($hasla, 1)];
        return $this->render('WisielecBundle:Default:gra.html.twig', array('wylosowane'=>$wylosowane));
    }
	
	/**
     * @Route("/filmy", name="filmy")
     */
    public function filmyAction()
    {
		$repository = $this->getDoctrine()->getRepository('WisielecBundle:Hasla');
		$hasla = $repository->findBy(array('idKategorii'=>4));
		$wylosowane = $hasla[array_rand($hasla, 1)];
        return $this->render('WisielecBundle:Default:gra.html.twig', array('wylosowane'=>$wylosowane));
    }
}
