<?php

namespace WisielecBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WisielecBundle\Entity\Hasla;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;

class FormController extends Controller
{
    /**
     * @Route("/dodaj_haslo", name="dodaj_haslo")
     */
    public function formAction(Request $request)
    {
		$haslo = new Hasla();
		
		$haslo->setIdKategorii(1);
		$haslo->setHaslo('');
		
		$form = $this->createFormBuilder($haslo)->add('haslo', TextType::class)->add('idKategorii', ChoiceType::class, array(
			'choices' => array(
				'Przysłowia' => 1,
				'Aktorzy' => 2,
				'Pisarze' => 3,
				'Filmy' => 4,
			),
		))->add('dodaj', SubmitType::class, array('label' => 'Dodaj Hasło'))->getForm();
		
		$form->handleRequest($request);
		
		if($form->isSubmitted() && $form->isValid()){
			$haslo = $form->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($haslo);
			$em->flush();
			/*
			unset($haslo);
			unset($form);
			$haslo = new Hasla();
			$form = $this->createFormBuilder($haslo)->add('haslo', TextType::class)->add('IdKategorii', ChoiceType::class, array(
			'choices' => array(
				'Przysłowia' => 1,
				'Aktorzy' => 2,
				'Pisarze' => 3,
				'Filmy' => 4,
			),
		))->add('dodaj', SubmitType::class, array('label' => 'Dodaj Hasło'))->getForm();*/
		$this->redirect($this->generateUrl('dodaj_haslo'));
		}
		
        return $this->render('WisielecBundle:Default:form.html.twig', array(
														'form' => $form->createView()
														));
    }
	
}
