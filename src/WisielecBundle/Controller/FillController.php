<?php

namespace WisielecBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="welcome")
     */
    public function indexAction()
    {
        return $this->render('WisielecBundle:Default:index.html.twig');
    }
	
	/**
     * @Route("/przyslowia", name="przyslowia")
     */
    public function przyslowiaAction()
    {
		
        return $this->render('WisielecBundle:Default:przyslowia.html.twig');
    }
	
	/**
     * @Route("/aktorzy", name="aktorzy")
     */
    public function aktorzyAction()
    {
        return $this->render('WisielecBundle:Default:aktorzy.html.twig');
    }
	
	/**
     * @Route("/pisarze", name="pisarze")
     */
    public function pisarzeAction()
    {
        return $this->render('WisielecBundle:Default:pisarze.html.twig');
    }
	
	/**
     * @Route("/filmy", name="filmy")
     */
    public function filmyAction()
    {
        return $this->render('WisielecBundle:Default:filmy.html.twig');
    }
}
